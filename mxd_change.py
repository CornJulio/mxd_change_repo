import arcpy, os

#set workspace
path = "Q:\\LMDB\\EDBP_Removal_Processing"

for r,d,f in os.walk(path):
    for m in f:
        if m.endswith(".mxd"):
            mxd = arcpy.mapping.MapDocument(os.path.join(r, m))
            #get dataframes
            dataframes = arcpy.mapping.ListDataFrames(mxd)
            for df in dataframes:
                layers = arcpy.mapping.ListLayers(mxd, "LMDB All EDBP Activities")
                for layer in layers:
                    print("LMDB EDBP Layer found in " + m)
                    #remove any layers in this list, as they contain LMDB ALL EDBP Activities
                    try:
                        arcpy.mapping.RemoveLayer(df, layer)
                        print(str(layer.name) + " removed from " + m)
                        #save the changes
                        mxd.save()
                        print("changes applied to " + m)
                        del mxd, layers
                    except:
                        print(m + " cannot be saved")

print("script complete")
